//
//  ViewController.swift
//  LocalNotifications
//
//  Created by anu Priya on 4/23/15.
//  Copyright (c) 2015 Softway. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBAction func handleNotificationBtn(sender: AnyObject) {
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "Testing notifications on iOS8"
        localNotification.alertBody = "Woww it works"
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 5)
        localNotification.category = "INVITE_CATEGORY"
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleModifyListNotification", name: "modifyListNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleDeleteListNotification", name: "deleteListNotification", object: nil)

    }

    func handleModifyListNotification() {
        println("inside edit")
    }
    func handleDeleteListNotification() {
        println("inside delete")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

